# SQL Injection Vulnerability Tests

This InSpec profile contains tests to check for SQL injection vulnerabilities in your web application. It simulates common SQL injection attacks and checks for potential vulnerabilities.

## Prerequisites

- [InSpec](https://www.inspec.io/) installed on your system.
- Docker (for running tests using the Docker image).

## getting started on a CI/CD


# coming soon

## Getting Started on a local machine


1. Clone this repository to your local machine:

   ```bash
   git clone https://github.com/your-username/sql-injection-profile.git
   cd sql-injection-profile
   ```

2. Customize the sql_injection_check.rb file to match your application's behavior and attack simulations.
3. Build the docker image ```docker build -t sql-injection-tests .``` 
4. ```docker run --rm -v $PWD:/path/to/inspec/profile sql-injection-tests inspec exec sql_injection_check.rb -t docker:/```

